var myApp = angular.module('delivery',[]);
myApp.controller('listController', ['$scope', '$http', function($scope, $http) {
    $http.get("https://6059fb9db11aba001745d43f.mockapi.io/api/v1/cards").then(function(response){
        $scope.items = response.data.items; 
    });
}]);